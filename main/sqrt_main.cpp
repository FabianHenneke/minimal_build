#include <iostream>
#include <string>

#include <ap_math/ap_math.h>

int main()
{
  std::string input;
  std::cout << "Enter a positive integer: ";
  std::cin >> input;
  Integer number(input);
  std::cout << "FloorSqrt(" << number << ")=" << FloorSqrt(number) << std::endl;
  return 0;
}
