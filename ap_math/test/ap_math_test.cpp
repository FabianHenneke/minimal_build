#include <gtest/gtest.h>

#include <ap_math/ap_math.h>

#include <stdexcept>

TEST(FloorSqrt, Trivial)
{
  EXPECT_EQ(0, FloorSqrt(0));
  EXPECT_THROW(FloorSqrt(-1), std::logic_error);
}

TEST(FloorSqrt, Square)
{
  EXPECT_EQ(1, FloorSqrt(1));
  EXPECT_EQ(2, FloorSqrt(4));
  EXPECT_EQ(5, FloorSqrt(25));
}

TEST(FloorSqrt, LargeNumbers)
{
  EXPECT_EQ(Integer("219873264732984732198"),
            FloorSqrt(Integer("48344252544341187598346926179373777911204")));
}

TEST(FloorSqrt, Floor)
{
  EXPECT_EQ(4, FloorSqrt(24));
  EXPECT_EQ(5, FloorSqrt(26));
}
