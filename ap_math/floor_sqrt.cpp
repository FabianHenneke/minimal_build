#include "ap_math/floor_sqrt.h"

#include <stdexcept>

Integer FloorSqrt(const Integer& x)
{
  if (x == 0) return 0;

  if (x < 0) throw std::invalid_argument("sqrt: input < 0");

  // Bound root between powers of two
  Integer upper_bound = 1;
  Integer lower_bound;
  while (upper_bound * upper_bound <= x) {
    lower_bound = upper_bound;
    upper_bound *= 2;
  }

  Integer root;
  // Binary search on the remaining interval [lower_bound, upper_bound)
  // Integer root = lower_bound;
  while (upper_bound - lower_bound != 1) {
    root = (lower_bound + upper_bound) /
           2;  // root is precisely floor((low+up)/2).
               // This only agrees with low once the interval has length 1,
               // so the algorithm terminates
    Integer square = root * root;
    if (square <= x) {
      lower_bound = root;
    } else {
      upper_bound = root;
    }
  }

  return lower_bound;
}
