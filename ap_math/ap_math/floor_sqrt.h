#pragma once

#include "types.h"

Integer FloorSqrt(const Integer& x);
